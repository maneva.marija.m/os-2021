package main;

import streams.IOStreamManager;
import streams.impl.IOStreamManagerImpl;

import java.io.File;
import java.io.IOException;

public class IOStreamMain {

    public static void main(String[] args) throws IOException {
        String filePath = "C:\\Users\\ana5t\\work\\teaching\\2020-2021\\summer\\OS\\tmp\\in.txt";
        String filePathDest = "C:\\Users\\ana5t\\work\\teaching\\2020-2021\\summer\\OS\\tmp\\out.txt";

        IOStreamManager manager = new IOStreamManagerImpl();
        manager.printContentOfTxtFile(new File(filePath), System.out);

        //manager.writeToTextFile(new File(filePathDest), "This is text 5.", false);
    }
}
